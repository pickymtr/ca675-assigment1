import os

print('The top 10 posts by score:')
os.system('hive -f src/hive_queries/query1.sql')

print('The top 10 users by post score:')
os.system('hive -f src/hive_queries/query2.sql')

print('The number of distinct users, who used the word Hadoop in one of their posts:')
os.system('hive -f src/hive_queries/query3.sql')
