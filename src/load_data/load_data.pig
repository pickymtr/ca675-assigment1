REGISTER /usr/lib/pig/piggybank.jar;
res = LOAD '/data/*.csv'
	USING org.apache.pig.piggybank.storage.CSVExcelStorage(',','YES_MULTILINE','NOCHANGE','SKIP_INPUT_HEADER')
	AS (id: int,  posttypeid: int,  acceptedanswerid: int,  parentid: int,  creationdate: Datetime, 
	deletiondate: Datetime,  score: int,  viewcount: int,  body: chararray,  owneruserid: int,  
	ownerdisplayname: chararray,  lasteditoruserid: int,  lasteditordisplayname: chararray,  lasteditdate: Datetime,  
	lastactivitydate: Datetime,  title: chararray,  tags: chararray,  answercount: int,  commentcount: int,  
	favoritecount: int,  closeddate: Datetime,  communityowneddate: Datetime,  contentlicense: chararray);
res = FOREACH res GENERATE id, score, viewcount, (REPLACE(body,'[\r\n\\"]+','')) AS body, owneruserid, title, tags;
STORE res INTO 'resTable'
   USING org.apache.hive.hcatalog.pig.HCatStorer();
