DROP TABLE IF EXISTS resTable;
CREATE TABLE IF NOT EXISTS resTable 
    (id Int, score Int, viewcount Int, body String, 
    owneruserid Int, title String, tags String);
