import os

os.system('hive -f src/load_data/create_db.sql')
print('resDb created')

os.system('hive -f src/load_data/create_table.sql')
print('resTable created')

os.system('pig -useHCatalog -x mapreduce src/load_data/load_data.pig')
print('LOAD data in resTable with PIG finished')